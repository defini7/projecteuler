def fibonacci(n):
    if n in (1, 2):
        return 1
    return fibonacci(n - 1) + fibonacci(n - 2)
    
i = 2
s = 0
while True:
    n = fibonacci(i)
    
    if n > 4_000_000:
        break
    
    if n % 2 == 0:
       s += n
       
    i += 1
    
print(s)

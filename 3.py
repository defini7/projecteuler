import math

def larget_prime_factor(n):
    m = 0
    
    while n % 2 == 0:
        m = max(m, n)
        n /= 2
        
    for i in range(3, int(math.sqrt(n)) + 1, 2):
        while n % i == 0:
            m = max(m, i)
            n /= i
    
    if n > 2:
        m = max(m, n)
        
    return m
    
print(larget_prime_factor(600851475143))
        

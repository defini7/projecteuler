sum_of_sq = sum([n*n for n in range(1, 101)])

sq_of_sum = sum([n for n in range(1, 101)])
sq_of_sum *= sq_of_sum

print(sq_of_sum - sum_of_sq)

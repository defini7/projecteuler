i = 0
while True:
    i += 20
    divisible = True
    for j in range(1, 21):
        if i % j != 0:
            divisible = False
    
    if divisible:        
        print(i)
        break
